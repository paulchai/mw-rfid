#Capturing Application: Quick Start


##Overview

This tutorial will guide you through an example scenario, how the Fosstrak Capturing Application can be used to link the Fosstrak ALE and the Fosstrak EPCIS repository by retrieving ECReports from the ALE and delivering EPCIS events to the EPCIS repository. The tutorial shows two different solutions to the scenario described below (A simple one and a more elaborate one).

##Scenario

We model a simple store equipped with three RFID readers. One reader (`Reader_GoodsReceiving`) in the backend, where incoming goods are registered by the staff. A second reader (`Reader_PointOfExit`) installed at the exit door that either warns about stolen goods or thanks the customer for the purchasing. The third reader (`Reader_PointOfSale`) is installed at the cash desk.

  * `Reader_GoodsReceiving`: Upon tag read: write a new entry into the EPCIS repository (new registered item).
  * `Reader_PointOfSale`: Upon tag read: check the EPCIS repository if the tag has already been sold. If not sold, create a new entry in the repository, flaging the tag as sold.
  * `Reader_PointOfExit`: Upon tag read: check the EPCIS repository if the tag has been sold. If not sold, raise an alarm.

##Prerequisites

  * a copy of the `fc-server-1.2.0.war`, [fc-server-1.2.0-bin-with-dependencies.zip](https://oss.sonatype.org/content/repositories/public/org/fosstrak/fc/fc-server/1.2.0/fc-server-1.2.0-bin-with-dependencies.zip) or newer
  * a copy of the `fc-webclient fc-webclient-1.2.0.war`, [fc-webclient-1.2.0-bin-with-dependencies.zip](https://oss.sonatype.org/content/repositories/public/org/fosstrak/fc/fc-webclient/1.2.0/fc-webclient-1.2.0-bin-with-dependencies.zip) or newer
  * a copy of the `epcis-repository-0.5.0.war`, [epcis-repository-0.5.0-bin-with-dependencies.zip](https://bitbucket.org/paulchai/mw-rfid/downloads/epcis-repository-0.5.0-bin-with-dependencies.zip) or newer
  * a copy of the `hal-simulator`. Select [hal-impl-sim-0.5.0-bin-with-dependencies](https://bitbucket.org/paulchai/mw-rfid/downloads/hal-impl-sim-0.5.0-bin-with-dependencies.tar.gz).[[zip](https://bitbucket.org/paulchai/mw-rfid/downloads/hal-impl-sim-0.5.0-bin-with-dependencies.zip)|[tar.gz](https://bitbucket.org/paulchai/mw-rfid/downloads/hal-impl-sim-0.5.0-bin-with-dependencies.tar.gz)] or newer
  * a copy of the `capturingapp-0.1.1.war`, [capturingapp-0.1.1.war](https://oss.sonatype.org/content/repositories/public/org/fosstrak/capturingapp/capturingapp/0.1.1/capturingapp-0.1.1.war)  or newer
  * Configuration files:
    * [Reader_GoodsReceiving.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_GoodsReceiving.xml) (both scenarios)
    * [Reader_PointOfExit.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_PointOfExit.xml) (both scenarios)
    * [Reader_PointOfSale.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_PointOfSale.xml) (both scenarios)
    * [SimulatorClient.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/SimulatorClient.xml) (both scenarios)
    * [SimulatorController.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/SimulatorController.xml) (both scenarios)
    * [EventCycle_GoodsReceiving.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/EventCycle_GoodsReceiving.xml) (simple scenario)
    * [EventCycle_PointOfExit.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/EventCycle_PointOfExit.xml) (simple scenario)
    * [EventCycle_PointOfSale.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/EventCycle_PointOfSale.xml) (simple scenario)
    * [fosstrakDemo.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/fosstrakDemo.xml) (elaborate scenario)
    * [SimpleWareHouse-3EventCycles.drl](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/EventCycle_PointOfSale.xml) (simple scenario)
    * [SimpleWareHouse-1EventCycle.drl](https://bitbucket.org/paulchai/mw-rfid/downloads/SimpleWareHouse-1EventCycle.drl) (elaborate scenario)
  * [Apache Tomcat](http://tomcat.apache.org/) Java Servlet Container and [Mysql](http://www.mysql.com/). If you do not have neither you can install [BitNami Tomcat Stack](http://bitnami.com/stack/tomcat/installer) which included Tomcat 7, Mysql 5.5, Mysql Connector/J 5.1 and phpMyAdmin.

##Preparations

The preparations need to be performed for both solutions.

###Start the multi reader simulator

We use a simulator to generate the tag input for the Filtering & Collection. The multi reader simulator is capable of simulating several readers in one application, allowing you to move tags from one reader to another.

First unpack `hal-impl-sim-VERSION-bin-with-dependencies` to a folder of your choice (example `c:\fosstrakDemo`).

you can also compile the [Source Code](http://fosstrak.googlecode.com/svn/hal/tags/hal-0.5.0/) using maven.

	cd c:\hal-0.5.0
	mvn -Declipse.workspace=<path-to-eclipse-workspace> eclipse:add-maven-repo
	mvn eclipse:eclipse
	mvn package
	cd c:\hal-0.5.0\hal-impl-sim\target

the `hal-impl-sim-VERSION-bin-with-dependencies` is located in `c:\hal-0.5.0\hal-impl-sim\target`

Open a command line and run the simulator.

	cd c:\fosstrakDemo
	java -cp hal-impl-sim-0.5.0\hal-impl-sim-0.5.0.jar org.fosstrak.hal.impl.sim.multi.SimulatorServerController


If everything runs fine, an empty window should pop up.

![](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/simulatorController.png)

###Prepare EPCIS repository

If you already have a running instance of an EPCIS repository, you can skip this step. Otherwise follow the [EpcisUserGuide installation instructions](https://code.google.com/p/fosstrak/wiki/EpcisUserGuide#How_to_Capture_EPCIS_Events) of the Fosstrak EPCIS repository. Or follow the step-by-step tutorial outlined below:

  * Download the Fosstrak EPCIS repository distribution and place the WAR file contained in the archive in your Tomcat's webapps directory. After restarting Tomcat, the WAR file will be exploded.

  * Make sure that web applications deployed to Tomcat can access your MySQL server by installing the MySQL [Connector/J](http://www.mysql.com/products/connector/j/) driver. This is usually done by copying the `mysql-connector-java-<version>-bin.jar` into Tomcat's `lib` (version 6) or `common/lib` (version 5.5) directory.

  * Set up a MySQL database for the EPCIS repository to use. Log into the MySQL Command Line Client as root and perform the following steps:<br/>

    * Create the database (in this example, we'll use epcis as the database name).
    
		`mysql> CREATE DATABASE epcis;`

    * Create a user that is allowed access to the newly created database (in this example, we'll use the user name _epcis_ and password _epcis_).
    
		`mysql> GRANT SELECT, INSERT, UPDATE, DELETE ON epcis.* TO epcis IDENTIFIED BY 'epcis';`

    * Create the database schema by running the setup script contained in the archive you downloaded. (Make sure you are connected to the newly created database before running the script.)
    
		`mysql> USE epcis;`
		
		`mysql> SOURCE <path-to-unpacked-download>/epcis_schema.sql`

    * Optionally populate the repository with some sample data.
    
		`mysql> SOURCE <path-to-unpacked-download>/epcis_demo_data.sql`

  * Configure the repository to connect to the newly created database. In a default installation of Tomcat, the database connection settings can be found in `$TOMCAT_HOME/conf/Catalina/localhost/epcis-repository-<version>.xml`. The relevant attributes that must be adjusted are _username_, _password_ and _url_.
  
		file: $TOMCAT_HOME/conf/Catalina/localhost/epcis-repository-0.5.0.xml
		
		<?xml version="1.0" encoding="ISO-8859-1"?>
		<Context reloadable="true">
			<Resource
			    name="jdbc/EPCISDB"
			    type="javax.sql.DataSource"
			    auth="Container"
			    username="epcis"
			    password="epcis"
			    driverClassName="com.mysql.jdbc.Driver"
			    url="jdbc:mysql://localhost:3306/epcis?autoReconnect=true">
			</Resource>
		</Context>


	If you used the default user name, password and database name from the examples above, then you don't need to reconfigure anything here. If, however, you used different values, you need to stop Tomcat, change the values and start Tomcat again.

  * Check if the application is running. In a default installation of Tomcat, the capture and query interfaces will now be available at `http://localhost:8080/epcis-repository-<version>/capture` and `http://localhost:8080/epcis-repository-<version>/query`, respectively. When you open the capture interface's URL in your web browser, you should see a short information page similar to this:
  
		This service captures EPCIS events sent to it using HTTP POST requests.
		The payload of the HTTP POST request is expected to be an XML document
		conforming to the EPCISDocument schema.
		
		For further information refer to the xml schema files or check the Example
		in 'EPC Information Services (EPCIS) Version 1.0 Specification', Section 9.6.
	
	To also check if the query interface is set up correctly, point your browser to its URL and append the string `?wsdl` to it. The WSDL file of the query service should now be displayed in your browser. Proceed to the next sections to test your repository installation using one of our client applications.

  * Check the application's log file in case of problems. The application's log is kept in `TOMCAT_HOME/logs/epcis-repository.log`. In case of problems with your own EPCIS repository instance, this is the first place to look for information about errors or specific exceptions thrown by the application.


###Prepare the Filtering & Collection

Deploy the Fosstrak Filtering & Collection to your Tomcat `webapps` folder (by simply copying the WAR file into the folder). We need access to the configuration files packed into the WAR file, so unpack the war-file into the `webapps` folder.

Copy the two configuration files `SimulatorClient.xml` and `SimulatorController.xml` into the folder `webapps\fc-server-VERSION\WEB-INF\classes\props` (overwrite the existing copies).

We recommend to adjust the log level for the Filtering & Collection from either `DEBUG` or `INFO` to `ERROR`. To do so, open the file `webapps\fc-server-VERSION\WEB-INF\classes\log4j.xml` and adjust the log level:


	file: <TOMCAT_FOLDER>\webapps\fc-server-VERSION\WEB-INF\classes\log4j.xml
	
	# class specific levels
	<category name="org.fosstrak">
		<priority value="ERROR" />
	</category>
	
	<category name="org.fosstrak.ale">
		<priority value="ERROR" />
	</category>
	
	<root>
		<priority value="ERROR" />
		<appender-ref ref="file" />
	</root>


##Simple Solution

This solution sets up an `EventCycle` for each reader. This allows the capturing application to distinguish between different readers by the name of the `EventCycle`.

Please ensure, that you followed the setup in Preparations.

Copy the `fc-webclient` and the `capturing-app` WAR files into the Tomcat `webapps` folder and unpack the capturing-app file (We need to adjust a few configuration parameters).

###Adjust the configuration

1. First, we define a new capturing application that is listening on port 9999 for incoming ECReports, and that is delivering EPCIS events to the EPCIS repository at `http://localhost:8080/epcis-repository-0.5.0/capture`. Open the file `webapps/capturing-app-VERSION/WEB-INF/classes/captureapplication.properties` and set the configuration below:

		file: <TOMCAT_FOLDER>/webapps/capturing-app-VERSION/WEB-INF/classes/captureapplication.properties
		
		n=1
		
		cap.0.port=9999
		cap.0.name=myFirstCaptureApp
		cap.0.epcis=http://localhost:8080/epcis-repository-0.5.0/capture

2. Second, we instruct the capturing application to use the simple solution for the scenario. Open the file `webapps/capturing-app-VERSION/WEB-INF/classes/changeset.xml` and set the configuration below:

		file: <TOMCAT_FOLDER>/webapps/capturing-app-VERSION/WEB-INF/classes/changeset.xml
		
		<change-set xmlns='http://drools.org/drools-5.0/change-set'
		            xmlns:xs='http://www.w3.org/2001/XMLSchema-instance'
		            xs:schemaLocation='http://drools.org/drools-5.0/change-set.xsd http://anonsvn.jboss.org/repos/labs/labs/jbossrules/trunk/drools-api/src/main/resources/change-set-1.0.0.xsd' >
		   <add>
		      <resource source='classpath:drools/SimpleWareHouse-3EventCycles.drl' type='DRL' />
		   </add>
		</change-set>
	
	You also need to modify the `epcis-repository` url address in the drl file:
	
	file: <TOMCAT_FOLDER>\webapps\capturingapp-VERSION\WEB-INF\classes\drools\SimpleWareHouse-3EventCycles.drl
		
		// only register if not in EPCIS yet
		$epcs : LinkedList( size > 0 ) from collect (
			EPC() from fosstrakDemoNotInEPCIS($rec, 
					"urn:fosstrak:demo:bizstep:receiving", 
					"urn:fosstrak:demo:disp:for_sale",
					"urn:fosstrak:demo:rp:Reader_GoodsReceiving",
					"urn:fosstrak:demo:loc:GoodsReceiving",
					"http://localhost:8080/epcis-repository-0.5.0/query")
			)
		...
		// only register if not in EPCIS yet
		$epcs : LinkedList( size > 0 ) from collect (
			EPC() from fosstrakDemoNotInEPCIS($rec, 
					"urn:fosstrak:demo:bizstep:sale", 
					"urn:fosstrak:demo:disp:sold",
					"urn:fosstrak:demo:rp:Reader_PointOfSale",
					"urn:fosstrak:demo:loc:PointOfSale",
					"http://localhost:8080/epcis-repository-0.5.0/query")
			)
		...
		// get the stolen ones...
		$stolen : LinkedList() from collect (
			EPC() from fosstrakDemoIsStolen(
				$epcs,
				"http://localhost:8080/epcis-repository-0.5.0/query")
			)
	
	Or copy the drools file `SimpleWareHouse-3EventCycles.drl` into the folder `webapps\capturingapp-VERSION\WEB-INF\classes\drools\` (overwrite the existing copies).
3. Start Tomcat now.

###Setup the readers

1. Start your web browser and point it to the address of the web-based client.

		http://<SERVER>:<PORT>/<WEBCLIENT_VERSION>/services/ALEWebClient.jsp
		
		example:
		http://localhost:8080/fc-webclient-1.2.0/services/ALEWebClient.jsp

1. Set the endpoint to the filtering and collection server's Logical Reader API by selecting `setEndPoint(String endPointName)` in the !LogicalReader API. Click "Invoke" to execute the command.

		endpoint: http://<SERVER>:<PORT>/<FCSERVER_VERSION>/services/ALELRService
		
		example: 
		http://localhost:8080/fc-server-1.2.0/services/ALELRService

3. Verify that a connection between the web-based client and the server can be established by clicking `getVendorVersion()`. A version number should be returned.
4. The next step is to establish a connection from the Filtering & Collection to the reader simulator (basically this means to setup three different readers).
5. Click on `define(String readerName, LRSpec spec)` in the section !LogicalReader API. Do not confuse the `define` method for an !EventCycle with the `define` method for a logical reader! Define three readers. Make sure to set the reader names exactly as they are written below.
    1. `Reader_GoodsReceiving` with [Reader_GoodsReceiving.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_GoodsReceiving.xml)
    2. `Reader_PointOfSale` with [Reader_PointOfSale.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_PointOfSale.xml)
    3. `Reader_PointOfExit` with [Reader_PointOfExit.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_PointOfExit.xml)

			Example:
			
			readerName: Reader_PointOfSale
			specFilePath: c:\fosstrakDemo\Reader_PointOfSale.xml

6. Retrieve the logical reader names to check if readers are there by clicking `getLogicalReaderNames()`. The result should be `[Reader_PointOfSale, Reader_PointOfExit, Reader_GoodsReceiving]`.
7. Now switch to the multi reader simulator and refresh it to display the readers (click "view" and then "refresh").

###Setup the EventCycles

1. Start your web browser and point it to the address of the web-based client.

		http://<SERVER>:<PORT>/<WEBCLIENT_VERSION>/services/ALEWebClient.jsp
		
		example:
		http://localhost:8080/fc-webclient-1.2.0/services/ALEWebClient.jsp

1. Set the endpoint to the filtering and collection server by selecting the `setEndpoint(String endPointName)` method in the Filtering and Collection API. Click "Invoke" to execute the command.

		endpoint: http://<SERVER>:<PORT>/<FCSERVER_VERSION>/services/ALEService
		
		example: 
		http://localhost:8080/fc-server-1.2.0/services/ALEService

2. Verify that a connection between the web-based client and the server can be established by clicking `getVendorVersion()`. A version number should be returned.
3. In the next step you will define three ALE ECSpecs. These tell the ALE Middleware how the RFID tag reads arriving from the simulator should be filtered and aggregated.
4. Invoke the method `define(String specName, String specFilePath)`. Do not confuse the `define` method for an !EventCycle with the `define` method for a logical reader. For this scenario we need to create three !EventCycles. Make sure to set the names of the !EventCycles exactly as they are written below:
    1. `EventCycle_GoodsReceiving` with spec [EventCycle_GoodsReceiving.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/EventCycle_GoodsReceiving.xml)
    2. `EventCycle_PointOfExit` with spec [EventCycle_PointOfExit.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/EventCycle_PointOfExit.xml)
    3. `EventCycle_PointOfSale` with spec [EventCycle_PointOfSale.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/EventCycle_PointOfSale.xml)

			Example:
			
			specName: EventCycle_PointOfExit
			specFilePath: c:\fosstrakDemo\EventCycle_PointOfExit.xml

5. Retrieve the !EventCycle names to check if they are created correctly by clicking `getECSpecNames()`. The result should be: `[EventCycle_PointOfSale, EventCycle_PointOfExit, EventCycle_GoodsReceiving]`.

###Deliver ECReports to the capturing application

Invoke `subscribe(String specName, String notificationUri)`. For each of the three !EventCycles we just created, invoke the registration with the target URL `http://localhost:9999`.

1. `EventCycle_GoodsReceiving`
2. `EventCycle_PointOfExit`
3. `EventCycle_PointOfSale`
    
		Example:
		
		notificationURI: http://localhost:9999
		specName: EventCycle_GoodsReceiving

###Watch the result

Now all the applications should be linked together correctly. Switch back to the multi reader simulator and make sure, that you can look at the log-output of Tomcat (the capturing application will print the scenario output to the commandline provided by Tomcat).

Create a few tags (please use the tags from the list below, as they can be translated by the ALE to a meaningful representation).

Tags:

  * `3078E7D4141ADC7C66FB10A5`
  * `30842E5663E88618294179E9`
  * `3025979BA9244C71AF8A92B6`
  * `30E85865E0CB78EA767BBBEC`
  * `307AD9A00C520D2585913539`
  * `305887668C7F5160AA3CB66F`

Move one tag on the reader `Reader_GoodsReceiving` and wait a few seconds. On the commandline a message will be printed informing you about newly registered items.

	file: <TOMCAT_FOLDER>/logs/[catalina.out|tomcat<VERSION>.stdout<DATE>.log]
	
	example:	
	
	=====================================================
	registering new items:
	urn:epc:raw:96:15001446348594317971238359205
	=====================================================

Move the tag on the reader `Reader_PointOfExit` and wait a few seconds. On the commandline alert messages will be printed, as the goods have taken to the exit without being payed for.

	example:
	
	=====================================================
	!!!!!!!!!!!!! FOUND STOLEN GOODS!!!!!!!!!!
	urn:epc:raw:96:15001446348594317971238359205
	=====================================================

Move the tag on the reader `Reader_PointOfSale` and wait a few seconds. On the commandline a message will inform you about a customer having purchased some goods.

	example:
	
	=====================================================
	customer purchased items:
	urn:epc:raw:96:15001446348594317971238359205
	=====================================================

Move the tag back on the reader `Reader_PointOfExit` and a friendly message will be sent to you.

	example:
	
	=====================================================
	Dear customer, thank you for your purchasing. Goodbye
	=====================================================

###Clean the entries in the EPCIS repository

If you want to run the demo with the same EPCs again, you need to clean out all the entries in the EPCIS repository. A clean and dirty way, how you can achieve this, is via the mysql command line.

With the MySQL-Commandline client, connect to the MySQL server. Switch into the EPCIS database `use epcis;`. Then delete all the events `delete from event_objectevent;`, or run [epcis_clean_all.sql](http://fosstrak.googlecode.com/svn/epcis/tags/epcis-0.5.0/epcis-repository/src/main/sql/epcis_clean_all.sql).

##Elaborate Solution

This solution sets up one !EventCycle retrieving tags from all the readers. Aside the tag information (EPC), the !EventCycle delivers also tag stats (with the name of the reader that read the EPC). With this information, the capturing application can determine the name of the reader delivering the EPC.

Please ensure, that you followed the setup in Preparations.

Copy the `fc-webclient` and the capturing-app WAR files into the Tomcat `webapps` folder and unpack the capturing-app file (We need to adjust a few configuration parameters).

###Adjust the configuration

1. First, we define a new capturing application that is listening on port 9999 for incoming ECReports, and that is delivering EPCIS events to the EPCIS repository at `http://localhost:8080/epcis-repository-0.5.0/capture`. Open the file `webapps/capturing-app-VERSION/WEB-INF/classes/captureapplication.properties` and set the configuration below:

		file: <TOMCAT_FOLDER>/webapps/capturing-app-VERSION/WEB-INF/classes/captureapplication.properties
		
		n=1
		
		cap.0.port=9999
		cap.0.name=myFirstCaptureApp
		cap.0.epcis=http://localhost:8080/epcis-repository-0.5.0/capture

2. Second, we instruct the capturing application to use the elaborate solution for the scenario. Open the file `webapps/capturing-app-VERSION/WEB-INF/classes/changeset.xml` and set the configuration below:

		file: <TOMCAT_FOLDER>/webapps/capturing-app-VERSION/WEB-INF/classes/changeset.xml
		
		<change-set xmlns='http://drools.org/drools-5.0/change-set'
		            xmlns:xs='http://www.w3.org/2001/XMLSchema-instance'
		            xs:schemaLocation='http://drools.org/drools-5.0/change-set.xsd http://anonsvn.jboss.org/repos/labs/labs/jbossrules/trunk/drools-api/src/main/resources/change-set-1.0.0.xsd' >
		   <add>
		      <resource source='classpath:drools/SimpleWareHouse-1EventCycle.drl' type='DRL' />
		   </add>
		</change-set>
	
	You also need to modify the `epcis-repository` url address in the drl file:
	
		file: <TOMCAT_FOLDER>\webapps\capturingapp-VERSION\WEB-INF\classes\drools\SimpleWareHouse-1EventCycle.drl
		// only register if not in EPCIS yet
		$epcs : LinkedList( size > 0 ) from collect (
			EPC() from fosstrakDemoNotInEPCIS($rec, 
					"urn:fosstrak:demo:bizstep:receiving", 
					"urn:fosstrak:demo:disp:for_sale",
					"urn:fosstrak:demo:rp:Reader_GoodsReceiving",
					"urn:fosstrak:demo:loc:GoodsReceiving",
					"http://localhost:8080/epcis-repository-0.5.0/query")
			)
		...
		// only register if not in EPCIS yet
		$epcs : LinkedList( size > 0 ) from collect (
			EPC() from fosstrakDemoNotInEPCIS($rec, 
					"urn:fosstrak:demo:bizstep:sale", 
					"urn:fosstrak:demo:disp:sold",
					"urn:fosstrak:demo:rp:Reader_PointOfSale",
					"urn:fosstrak:demo:loc:PointOfSale",
					"http://localhost:8080/epcis-repository-0.5.0/query")
			)
		...
		// get the stolen ones...
		$stolen : LinkedList() from collect (
			EPC() from fosstrakDemoIsStolen(
				$epcs,
				"http://localhost:8080/epcis-repository-0.5.0/query")
			)
	
	Or copy the drools file `SimpleWareHouse-1EventCycle.drl` into the folder `webapps\capturingapp-VERSION\WEB-INF\classes\drools\` (overwrite the existing copies).
3. Start Tomcat now.

###Setup the readers

1. Start your web browser and point it to the address of the web-based client.

		http://<SERVER>:<PORT>/<WEBCLIENT_VERSION>/services/ALEWebClient.jsp
		
		example:
		http://localhost:8080/fc-webclient-1.2.0/services/ALEWebClient.jsp

2. Set the endpoint to the filtering and collection server's Logical Reader API by selecting `setEndPoint(String endPointName)` in the !LogicalReader API. Click Invoke to execute the command.

		endpoint: http://<SERVER>:<PORT>/<FCSERVER_VERSION>/services/ALELRService
		
		example: 
		http://localhost:8080/fc-server-1.2.0/services/ALELRService

3. Verify that a connection between the web-based client and the server can be established by clicking `getVendorVersion()`. A version number should be returned.
4. The next step is to establish a connection from the Filtering & Collection to the reader simulator (basically this means to setup three different readers).
5. Click on `define(String readerName, LRSpec spec)` in the section !LogicalReader API. Do not confuse the `define` method for an !EventCycle with the `define` method for a logical reader.
6. Define three readers. Make sure to set the reader names exactly as they are written below.
    1. `Reader_GoodsReceiving` with [Reader_GoodsReceiving.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_GoodsReceiving.xml)
    2. `Reader_PointOfSale` with [Reader_PointOfSale.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_PointOfSale.xml)
    3. `Reader_PointOfExit` with [Reader_PointOfExit.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/Reader_PointOfExit.xml)

			Example:
			
			readerName: Reader_PointOfSale
			specFilePath: c:\fosstrakDemo\Reader_PointOfSale.xml

7. Retrieve the logical reader names to check if readers are there by clicking `getLogicalReaderNames()`. The result should be `[Reader_PointOfSale, Reader_PointOfExit, Reader_GoodsReceiving]`.
8. Now switch to the multi reader simulator and refresh it to to display the readers (click "view" and then "refresh").

###Setup the EventCycle

1. Start your web browser and point it to the address of the web-based client.

		http://<SERVER>:<PORT>/<WEBCLIENT_VERSION>/services/ALEWebClient.jsp
		
		example:
		http://localhost:8080/fc-webclient-1.2.0/services/ALEWebClient.jsp

2. Set the endpoint to the filtering and collection server by selecting the `setEndpoint(String endPointName)` method in the Filtering and Collection API. Click "Invoke" to execute the command.

		endpoint: http://<SERVER>:<PORT>/<FCSERVER_VERSION>/services/ALEService
		
		example: 
		http://localhost:8080/fc-server-1.2.0/services/ALEService

3. Verify that a connection between the web-based client and the server can be established by clicking `getVendorVersion()`. A version number should be returned.
4. In the next step you will define one ALE ECSpec. This one tells the ALE Middleware how the RFID tag reads arriving from the simulator should be filtered and aggregated.
5. Invoke the method `define(String specName, String specFilePath)`. Do not confuse the `define` method for an !EventCycle with the `define` method for a logical reader.
6. For this scenario we need to create one !EventCycle. Make sure to set the name of the !EventCycle exactly as it's written below:
7. `fosstrakDemo` with spec [fosstrakDemo.xml](http://fosstrak.googlecode.com/svn/capturingapp/trunk/src/site/resources/files/quickstart/fosstrakDemo.xml)

		Example:
		
		specName: fosstrakDemo
		specFilePath: c:\fosstrakDemo\fosstrakDemo.xml

8. Retrieve the !EventCycle names to check if they are created correctly by clicking `getECSpecNames()`. The result should be: `[fosstrakDemo]`.

###Deliver ECReports to the capturing application

Invoke `subscribe(String specName, String notificationUri)`. Subscribe the capturing application to the results of the event cycle.

	Example:
	
	notificationURI: http://localhost:9999
	specName: fosstrakDemo

###Watch the result

Now all the applications should be linked together correctly. Switch back to the multi reader simulator and make sure, that you can look at the log-output of Tomcat (the capturing application will print the scenario output to the commandline provided by Tomcat).

Create a few tags (please use the tags from the list below, as they can be translated by the ALE to a meaningful representation).

Tags:

  * `3078E7D4141ADC7C66FB10A5`
  * `30842E5663E88618294179E9`
  * `3025979BA9244C71AF8A92B6`
  * `30E85865E0CB78EA767BBBEC`
  * `307AD9A00C520D2585913539`
  * `305887668C7F5160AA3CB66F`

Move one tag on the reader `Reader_GoodsReceiving` and wait a few seconds. On the commandline a message will be printed informing you about newly registered items.

	file: <TOMCAT_FOLDER>/logs/[catalina.out|tomcat<VERSION>.stdout<DATE>.log]
	
	example:
	
	=====================================================
	registering new items:
	urn:epc:raw:96:15001446348594317971238359205
	=====================================================

Move the tag on the reader `Reader_PointOfExit` and wait a few seconds. On the commandline alert messages will be printed, as the goods have taken to the exit without being payed for.

	example:
	
	=====================================================
	!!!!!!!!!!!!! FOUND STOLEN GOODS!!!!!!!!!!
	urn:epc:raw:96:15001446348594317971238359205
	=====================================================

Move the tag on the reader `Reader_PointOfSale` and wait a few seconds. On the commandline a message will inform you about a customer having purchased some goods.

	example:
	
	=====================================================
	customer purchased items:
	urn:epc:raw:96:15001446348594317971238359205
	=====================================================

Move the tag back on the reader `Reader_PointOfExit` and a friendly message will be sent to you.

	example:
	
	=====================================================
	Dear customer, thank you for your purchasing. Goodbye
	=====================================================  

###Clean the entries in the EPCIS repository

If you want to run the demo with the same EPCs again, you need to clean out all the entries in the EPCIS repository. A clean and dirty way, how you can achieve this, is via the mysql command line.

With the MySQL command line client, connect to the MySQL server. Switch into the EPCIS database `use epcis;`. Then delete all the events `delete from event_objectevent;`, or run [epcis_clean_all.sql](http://fosstrak.googlecode.com/svn/epcis/tags/epcis-0.5.0/epcis-repository/src/main/sql/epcis_clean_all.sql).