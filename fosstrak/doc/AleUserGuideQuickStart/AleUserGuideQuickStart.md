# User Guide: Quick Start
_Deploying the Fosstrak ALE Middleware with an LLRP-Compliant RFID Reader_

## Objective

This tutorial will guide you through an example that shows how the Fosstrak ALE Middleware is configured to collect data from an LLRP-compliant RFID reader.

## Prerequisites

You will need the following files:

  * a copy of the fc-server-1.2.0.war, [fc-server-1.2.0-bin-with-dependencies.zip](https://oss.sonatype.org/content/repositories/public/org/fosstrak/fc/fc-server/1.2.0/fc-server-1.2.0-bin-with-dependencies.zip) or higher. (note: LLRP support requires >= 1.0.0).
  * a copy of the fc-webclient fc-webclient-1.2.0.war, [fc-webclient-1.2.0-bin-with-dependencies.zip](https://oss.sonatype.org/content/repositories/public/org/fosstrak/fc/fc-webclient/1.2.0/fc-webclient-1.2.0-bin-with-dependencies.zip) or higher
  * a copy of the reader protocol client version 0.5.0, Select [reader-rp-client-0.5.0-bin-with-dependencies](https://bitbucket.org/paulchai/mw-rfid/downloads/reader-rp-client-0.5.0-bin-with-dependencies.tar.gz).[[zip](https://bitbucket.org/paulchai/mw-rfid/downloads/reader-rp-client-0.5.0-bin-with-dependencies.zip)|[tar.gz](https://bitbucket.org/paulchai/mw-rfid/downloads/reader-rp-client-0.5.0-bin-with-dependencies.tar.gz)] or higher (any other HTTP monitor that displays incoming HTTP request will work, too).
  * a copy of this LLRP [ADD_ROSPEC message](http://fosstrak.googlecode.com/svn/wikires/ale/ROSPEC_example.llrp)
  * a copy of this ALE [ECSpec message](http://fosstrak.googlecode.com/svn/wikires/ale/ECSpec_current.xml)
  * a copy of this ALE [logical reader definition file](http://fosstrak.googlecode.com/svn/wikires/ale/LLRPReader.xml)

You will also need to install the following programs if they are not already present on your machine:

  * Fosstrak LLRP Commander v1.1 on Eclipse Juno (4.2.1)., follow the [installation guide](https://code.google.com/p/fosstrak/wiki/LlrpInstallation)
  * [Rifidi Emulator V1.6.0](http://sourceforge.net/projects/rifidi/files/Rifidi%20Emulator/)
  * [Apache Tomcat Java Servlet Container v7.0.73](http://tomcat.apache.org/)

## Prepare Tomcat

Install Apache Tomcat if you have not installed it yet.

Copy the two WAR files (fc-server-_VERSION_.war and fc-webclient-_VERSION_.war) into the Tomcat's _webapps_ directory and start Tomcat. Tomcat will automatically deploy the two WAR files for you.


## Setup LLRP reader emulator

To simplify this demo, we use a reader emulator to generate tag reads.

  * Install the [Rifidi emulator](http://sourceforge.net/projects/rifidi/files/Rifidi%20Emulator/) (if needed)
  * Start the Rifidi emulator
  * Instantiate a new reader of type `LLRPReader` and name it `LLRPReader1` that listens on port 10101 and 5084 (default) using the Reader Wizard
  * Start the reader just instantiated, right click on the reader icon in the `Reader Viewer` and click `Start Reader`. it should show `LLRPReader1 (running)`

We will return to the LLRP reader emulator once the other components are configured. 

## Setup GUI for incoming HTTP ALE notifications 

To display the reports with the aggregated data sent by the filtering and collection server, we will start a Fosstrak tool that displays incoming HTTP requests. Make sure the provided port is not used by some other application.

	java -cp <READER_RP_CLIENT_VERSION>.jar org.fosstrak.reader.rp.client.EventSinkUI <PORT>
	
	example:
	java -cp reader-rp-client-0.5.0.jar org.fosstrak.reader.rp.client.EventSinkUI 9999

The screen will remain empty until the configuration of the filtering and collection server is complete. 

if you cannot start the sink ui, please make sure port 9999 is available, stop tomcat server may help too.

## Configure the Fosstrak ALE web-client

In this example, we will use the web-based client to configure the filtering and collection server. Before we can use the web-based client, we need to tell it the URL through which the filtering and collection server can be accessed.

Start your web browser and point it to the address of the web-based client.

	http://<SERVER>:<PORT>/<WEBCLIENT_VERSION>/services/ALEWebClient.jsp
	
	example:
	http://localhost:8080/fc-webclient-1.2.0/services/ALEWebClient.jsp

In the next step, specify two endpoints that tell the web-based client where the `Filtering and Collection API` and the `Logical Reader API` can be found.

Set the endpoint to the filtering and collection server by selecting the `setEndpoint(String endPointName)` method in the `Filtering and Collection API`. Click `Invoke` to execute the command.

	endpoint: http://<SERVER>:<PORT>/<FCSERVER_VERSION>/services/ALEService
	
	example: 
	http://localhost:8080/fc-server-1.2.0/services/ALEService

Verify that a connection between the web-based client and the server can be established by clicking `getVendorVersion()`. A version number should be returned.

Set the endpoint to the filtering and collection server's Logical Reader API by selecting `setEndPoint(String endPointName)` in the `LogicalReader API`. Click `Invoke` to execute the command.

	endpoint: http://<SERVER>:<PORT>/<FCSERVER_VERSION>/services/ALELRService
	
	example: 
	http://localhost:8080/fc-server-1.2.0/services/ALELRService

Verify that a connection between the web-based client and the server can be established by clicking `getVendorVersion()`. A version number should be returned. 

## Set the readers connected to ALE Middleware via the ALE Logical Reader API

The next step is to configure the fc-server with the LLRP reader connected. Click on `define(String readerName, LRSpec spec)` in the section !LogicalReader API.

DO NOT CONFUSE the define method for an !EventCycle with the define method for a logical reader! For this tutorial name your reader `LogicalReader1` and use the [LLRPReader.xml](http://fosstrak.googlecode.com/svn/wikires/ale/LLRPReader.xml) as your LRSpec.

	readerName:  <READERNAME>
	specFilePath: <PATH_TO_SPEC>\<SPEC_NAME>.xml
	
	example:
	readerName: LogicalReader1
	specFilePath: c:\epc\LLRPReader.xml

Verify that the reader has been created by clicking on `getLogicalReaderNames()`. The call should return a list of logical readers in brackets. Make sure that the reader just creates is listed.

In the Rifidi emulator you can now see the message exchange with the Fosstrak ALE middleware.

You can inspect the logical reader specification LRSpec by the method `getLRSpec(String readerName)`.

## Define the Filtering and Collection Behavior via the ALE Filtering and Collection API

In the next step you will define an ALE ECSpec. This tells the ALE Middleware how the RFID tag reads ariving from the Rifidi Emulator should be filtered and aggregated.

Invoke the method `define(String specName, String specFilePath)`. DO NOT CONFUSE the define method for an !EventCycle with the define method for a logical reader! For this tutorial name your !EventCycle `specCURRENT` and load this [ECSpec message](http://fosstrak.googlecode.com/svn/wikires/ale/ECSpec_current.xml)

	specName: <SPEC_NAME>
	specFilePath: <PATH_TO_SPEC>\<SPEC_NAME>.xml
	
	example:
	specName: specCURRENT
	specFilePath: c:\epc\ECSpec_current.xml

Verify the correct defintion of your ECSpec by invoking the method `getECSpecNames()`. You should get a list of ECSpec names currently defined in brackets. Make sure that the ECSpec you just defined is listed. 

## Specify the Event Consumer of the ALE Events Being Sent

When there is no subscriber for an ECSpec, the ECSpec is not executed. We therefore need to specify a listener by subscribing our event sink to the ECSpec `specCURRENT` we added earlier.

Invoke `subscribe(String specName, String notificationUri)` and register the URL on which the event sink GUI is listening.

	notificationURI: http://<SERVER>:<PORT>
	specName: <SPEC_NAME>
	
	example:
	notificationURI: http://localhost:9999
	specName: specCURRENT

The ALE will start sending empty ECReports to the event sink GUI as the Rifidi emulator is not configured to send EPC tag reads via the LLRP protocol yet.

## Configure the Rifidi Emulator to Report Tags in "Range"

1. Start the LLRP Commander (via Eclipse) and connect to the remote adapter instance running on the ALE. 
2. If you cannot start the LLRP Commander, you may need to stop tomcat server first and try again.
3. Check, if you can send and retrieve messages from the reader by sending a GET_READER_CAPABILITIES message. 
In the `Reader Explorer->Adapter->DEFAULT`, right click on the `LLRPReader1(connected)->Send Message`
4. Create a new LLRP message and replace the content by the content from the LLRP .
5. To add message, drag and drop [ROSPEC_example.llrp](http://fosstrak.googlecode.com/svn/wikires/ale/ROSPEC_example.llrp) file to `LLRP_CMDR->Draft` in the `Navigator View`
6. Send the message to the Rifid Emulator. Open the message(ROSPEC_example) in the Navigator and click on the red icon LLRP in the eclipse task bar. A dialog box pops up, where you can select the reader(DEFAULT|LLRPReader1), click the send button to send the message.
7. select Send `ENABLE_RO_SPEC` message to instruct the reader to enable the ROspec just loaded.
8. Disconnect the reader(LLRPReader1).
9. Start Tomcat server.

Now the virtual reader is ready to deliver tags to the ALE.

Switch to the Rifidi reader emulator and create a tag (SGTIN96, GEN2). When you place the tag on the reader antenna, the tag reads will be reported to the Fosstrak ALE Middleware. In The Rifidi Emulator LLRPReader1 console output, it will show the tag information with green background colour.

	BYTES (size: 73):  
	04 3D 00 00 00 49 00 00 00 00 00 F0 00 3F 8D 30 35 C8 37 FD CF EE F4 C8 AB 48 25 89 00 00 00 01 8E 00 00 8A 00 00 81 00 01 86 00 87 00 00 82 00 04 DD EC 8F 2C F4 58 84 00 04 DD EC 8F B5 98 D0 88 00 12 8B 00 00 8C 00 00 
	XML: 
	<?xml version="1.0" encoding="UTF-8"?>
	<Message type="ROAccessReport">
	   <TagReportData>
	      <EPC96>
	         <EPC value="30 35 C8 37 FD CF EE F4 C8 AB 48 25 " />
	      </EPC96>
	      <ROSpecID>
	         <ROSpecID value="1" />
	      </ROSpecID>
	      <SpecIndex>
	         <SpecIndex value="0" />
	      </SpecIndex>
	      <InventoryParameterSpecID>
	         <InventoryParamSpecID value="0" />
	      </InventoryParameterSpecID>
	      <AntennaID>
	         <AntennaID value="1" />
	      </AntennaID>
	      <PeakRSSI>
	         <PeakRSSI value="0" />
	      </PeakRSSI>
	      <ChannelIndex>
	         <ChannelIndex value="0" />
	      </ChannelIndex>
	      <FirstSeenTimestampUTC>
	         <Microseconds value="1369907990951000" />
	      </FirstSeenTimestampUTC>
	      <LastSeenTimestampUTC>
	         <Microseconds value="1369907999906000" />
	      </LastSeenTimestampUTC>
	      <TagSeenCount>
	         <TagCount value="18" />
	      </TagSeenCount>
	      <C1G2CRC>
	         <CRC value="0" />
	      </C1G2CRC>
	      <C1G2PC>
	         <PC value="0" />
	      </C1G2PC>
	   </TagReportData>
	</Message>

After the tag reads are filtered and collected as specified in the ECSpec, they are delivered by the Fosstrak ALE middleware to the event sink GUI. 

	example:
	POST / HTTP/1.1
	Host: localhost:9999
	Content-Type: text/xml; charset="utf-8"
	Content-Length: 602
	
	<?xml version="1.0" encoding="UTF-8"?>
	<ns2:ECReports xmlns:ns2="urn:epcglobal:ale:xsd:1" terminationCondition="DURATION" totalMilliseconds="9500" ALEID="ETHZ-ALE1820697867" date="2012-01-30T01:01:00.000+00:00" specName="specCURRENT">
	   <reports>
	      <report>
	         <group>
	            <groupList>
	               <member>
	                  <epc>urn:epc:id:sgtin:7474687.475067.226704967717</epc>
	                  <tag>urn:epc:tag:sgtin-96:1.7474687.475067.226704967717</tag>
	                  <rawHex>urn:epc:raw:96.x3035C837FDCFEEF4C8AB4825</rawHex>
	                  <rawDecimal>urn:epc:raw:96.14920299046020735974997968933</rawDecimal>
	               </member>
	            </groupList>
	         </group>
	      </report>
	   </reports>
	</ns2:ECReports>